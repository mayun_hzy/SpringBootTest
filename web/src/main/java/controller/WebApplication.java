package controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author huangzhaoyang
 * @version 1.0
 * @classname WebApplication
 * @description web子模块的Application启动类!!!
 * @date 2019/7/19 14:04
 */
@RestController
@SpringBootApplication
public class WebApplication {

    //主方法
    public static void main(String[] args) {
        SpringApplication.run(WebApplication.class, args);
    }

    //测试方法
    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String test() {
        return "hzy666";
    }
}
